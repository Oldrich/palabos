#include "palabos3D.h"
#include "palabos3D.hh"
#include <iostream>
#include "../../myLibs/headers3D.h"

using namespace plb;
using namespace std;

typedef double T;
#define D descriptors::ForcedD3Q19Descriptor

const plint maxIter  = 7e6;
const plint infoEvery = 50;
const plint statisticsEvery = 500;
const plint imageEvery = 500;
const plint vtkEvery = 500;

const T lb1m = 50;
const T lb1sec = 1000;

const T rhoC = 2300;

const T plVisc = 100.0 / rhoC * lb1m * lb1m / lb1sec;
const T yield = 100.0 / rhoC * lb1m * lb1m / lb1sec / lb1sec;

const T gravity = 9.8 * lb1m / lb1sec / lb1sec;

const T inletR = 0.05 * lb1m;
const T inletH0 = 10 * lb1m;
const T inletH1 = 10 * lb1m;

const plint inletCX = inletR + 0.5;
const plint inletCY = inletR + 0.5;
const plint inletCZ = 0.5 + inletH0 + inletH1 + 1;

const plint nx = ceil(2 * inletR) + 3;
const plint ny = ceil(2 * inletR) + 3;
const plint nz = inletCZ + 2;

class IsInBB : public DomainFunctional3D {
public:
    virtual bool operator() (plint ix, plint iy, plint iz) const {
        const T r = sqrt(pow(ix - inletCX, 2) + pow(iy - inletCY, 2));
        return r > inletR || iz == 0 || iz == nz - 1;
    }
    virtual IsInBB* clone() const { return new IsInBB(*this); }
};

int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::directories().setOutputDir("./tmp/");
    
    makeDir("tmp");
    makeDir("tmp/vtk");
    
    plb_ofstream pinfo("tmp/_info.txt");
    pcout.getOriginalStream().rdbuf(pinfo.getOriginalStream().rdbuf());
    
    Bingham::omegaMin =  1. / 5.;
    Bingham::plasticViscosity = plVisc;
    Bingham::yieldStress = yield;
    
    MultiBlockLattice3D<T,D> lt(nx, ny, nz, new IncGuoExternalForceBGKdynamics<T,D>(1.0));
    
    defineDynamics(lt, lt.getBoundingBox(), new IsInBB, new BounceBack<T,D>);
    
    setExternalVector(lt, lt.getBoundingBox(), D<T>::ExternalField::forceBeginsAt, Array<T,3>(0,0,-gravity)); 
    
    lt.initialize();
    
    OnLatticeBoundaryCondition3D<T,D>* bc = createZouHeBoundaryCondition3D<T,D>();
      
    const Box3D inletDomain(
        0, nx - 1,
        0, ny - 1,
        inletCZ, inletCZ);
    
    bc->addPressureBoundary2P(inletDomain, lt);
    setBoundaryDensity(lt, inletDomain, 1.0);

    plb_ofstream pumax("tmp/umax.txt");
    plb_ofstream pInflowRho("tmp/inflowRho.txt");
    plb_ofstream pInflowUz("tmp/inflowUz.txt");
    
    for (int iT = 0; iT < maxIter; ++iT) {
        
        if (iT % infoEvery == 0) {
            pcout << "---" << iT << "---" << endl;
            const double dur = global::timer("iteration").stop();
            global::timer("iteration").restart();
            pcout << dur << " sec" << endl;
        }
        
        if(iT % statisticsEvery == 0) {
            const Box3D lineIn(inletCX, inletCX, inletCY, inletCY, 0, nz-1);
            pInflowRho << *computeDensity(lt, lineIn) << endl;
            pInflowUz << *computeVelocityComponent(lt, lineIn, 2) << endl;
            pumax << iT << "\t" << computeMax(*computeVelocityNorm(lt)) << endl;
        }
        
        if(iT % vtkEvery == 0) {
            VtkImageCellOutput3D<T> vtkInlet(createFileName("vtk/inlet", iT, 6), 1.);
            vtkInlet.writeData<float>(*computeDensity(lt), "rho", 1.);
            vtkInlet.writeData<float>(*computeOmega(lt), "omega", 1.);
            vtkInlet.writeData<3,float>(*computeVelocity(lt), "velocity", 1);
        }
        
        lt.collideAndStream();
    }
    
    delete bc;
    return 0;
}