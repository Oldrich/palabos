#include "palabos3D.h"
#include "palabos3D.hh"
#include <cstdlib>
#include <iostream>
#include <map>

//#define linux
#ifdef linux
#include <sys/stat.h>
void makeDir(const char* name) { mkdir(name, 0777); }
#else
#include <direct.h>
void makeDir(const char* name) { mkdir(name); }
#endif

using namespace plb;
using namespace std;

typedef double T;
#define D descriptors::ForcedShanChenD3Q19Descriptor

const plint maxIter  = 7e6;
const plint infoEvery = 100;
const plint statisticsEvery = 500;
const plint imageEvery = -1;
const plint vtkEvery = 500;

const plint envelopeWidth = 1;
const plint basinBlocksX = 5;
const plint basinBlocksY = 5;
const plint inletBlocksZ = 5;

const T lb1m = 25;
const T lb1sec = 1000;
const T rho1 = 1000;

const T wRho = 1000; // kg/m3
const T bRho = 2300; // kg/m3

const T plVisc = 200.0 / bRho * lb1m * lb1m / lb1sec;
const T yield = 100.0 / bRho * lb1m * lb1m / lb1sec / lb1sec;

const T wVisc = 0.07;
const T wOmega = 1.0 / (wVisc * D<T>::invCs2 + 0.5);

const T gravity = 9.8 * lb1m / lb1sec / lb1sec; // m/s2

const T basinR = 3 * lb1m; // m
const T basinH = 0.5 * lb1m; // m

const T inletR = 0.1 * lb1m; // m
const T inletCXi = 0.65 * basinR; // m
const T inletCYi = basinR; // m
const T inletH0 = 20 * lb1m; // m
const T inletH1 = 20 * lb1m; // m

const T outlet1CX = 0.2 * lb1m; // m
const T outlet1CY = basinR; // m
const T outlet1R = 0.1 * lb1m; // m

const T outlet2CX = 2 * basinR - 0.2 * lb1m; // m
const T outlet2CY = basinR; // m
const T outlet2R = outlet1R; // m

const T outlet3CX = basinR; // m
const T outlet3CY = 0.2 * lb1m; // m
const T outlet3R = outlet1R; // m

const T outlet4CX = basinR; // m
const T outlet4CY = 2 * basinR - 0.2 * lb1m; // m
const T outlet4R = outlet1R; // m

const T outletH = 0.1 * lb1m; //m

const T rhoOutlet = 1 + (inletH1 - outletH) * D<T>::invCs2 * gravity * wRho / rho1;

const plint nx = ceil(2 * basinR) + 2;
const plint ny = ceil(2 * basinR) + 2;
const plint nz = ceil(basinH + inletH0 + inletH1) + 2;

const plint imageHeight = nz - 1;

const T inletCX = round(inletCXi + 0.5) - 0.5;
const T inletCY = round(inletCYi + 0.5) - 0.5;

const plint basinTop = round(basinH + 1);
const plint outletTop = round(basinTop + outletH);

const plint inletCZ = nz - 2;
const plint outletCZ = outletTop - 1;

T getUz(MultiBlockLattice3D<T, D> lt, plint ix, plint iy, plint iz) {
    Array<T,D<T>::d> velocity;
    lt.get(ix,iy,iz).computeVelocity(velocity);
    return velocity[2];
}

bool isInInlet(plint ix, plint iy, plint iz) {
    T r = sqrt(pow(ix - inletCX, 2) + pow(iy - inletCY, 2));
    return iz >= basinTop && iz < nz - 1 && r < inletR + 1e-3;
}

bool isBelowInlet(plint ix, plint iy, plint iz) {
    T r = sqrt(pow(ix - inletCX, 2) + pow(iy - inletCY, 2));
    return iz > 0 && iz < basinTop && r < 1.3*inletR;
}

bool isInBasin(plint ix, plint iy, plint iz) {
    T r = sqrt(pow(ix - basinR - 0.5, 2) + pow(iy - basinR - 0.5, 2));
    return iz > 0 && iz < basinTop && r < basinR + 1e-3;
}

bool isInOutlet(T outletCX, T outletCY, T outletR, plint ix, plint iy, plint iz) {
    T r = sqrt(pow(ix - outletCX - 0.5, 2) + pow(iy - outletCY - 0.5, 2));
    return iz >= basinTop && iz < outletTop && r < outletR + 1e-3; 
}

bool isInOutlet1(plint ix, plint iy, plint iz) { return isInOutlet(outlet1CX, outlet1CY, outlet1R, ix, iy, iz); }
bool isInOutlet2(plint ix, plint iy, plint iz) { return isInOutlet(outlet2CX, outlet2CY, outlet2R, ix, iy, iz); }
bool isInOutlet3(plint ix, plint iy, plint iz) { return isInOutlet(outlet3CX, outlet3CY, outlet3R, ix, iy, iz); }
bool isInOutlet4(plint ix, plint iy, plint iz) { return isInOutlet(outlet4CX, outlet4CY, outlet4R, ix, iy, iz); }

bool isInWater(plint ix, plint iy, plint iz) {
    return (isInBasin(ix,iy,iz) && !isBelowInlet(ix,iy,iz)) || isInOutlet1(ix,iy,iz) ||
            isInOutlet2(ix,iy,iz) || isInOutlet3(ix,iy,iz) || isInOutlet4(ix,iy,iz);
}

bool isInBeton(plint ix, plint iy, plint iz) {
    return isInInlet(ix,iy,iz) || isBelowInlet(ix,iy,iz);
}

bool isInBB(plint ix, plint iy, plint iz) {
    return !isInWater(ix,iy,iz) && !isInBeton(ix,iy,iz);
}

T omegaMin, omegaSum, omegaMax;
plint omegaN;

class OmegaMinMeanMax : public BoxProcessingFunctional3D_S<T> {
public:
    OmegaMinMeanMax* clone() const { return new OmegaMinMeanMax(*this); }
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const { modified[0] = modif::staticVariables; };
    virtual BlockDomain::DomainT appliesTo() const { return BlockDomain::bulk; }
    virtual void process(Box3D domain, ScalarField3D<T>& field) {
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
        for (plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
            if(isInBeton(iX, iY, iZ)) {
                T v = field.get(iX, iY, iZ);
                if (v > omegaMax) { omegaMax = v; }
                if (v < omegaMin) { omegaMin = v; }
                omegaSum += v;
                omegaN++;
            };
        }}}
    }
};

class IsInBB : public DomainFunctional3D {
public:
    IsInBB* clone() const { return new IsInBB(*this); }
    virtual bool operator() (plint ix, plint iy, plint iz) const { return isInBB(ix, iy, iz); }
};

class IsInBBW : public DomainFunctional3D {
public:
    IsInBBW* clone() const { return new IsInBBW(*this); }
    virtual bool operator() (plint ix, plint iy, plint iz) const {
        return !isInWater(ix,iy,iz) && !isInBasin(ix,iy,iz);
    }
};

class IsInInlet : public DomainFunctional3D {
public:
    IsInInlet* clone() const { return new IsInInlet(*this); }
    virtual bool operator() (plint ix, plint iy, plint iz) const { return isInInlet(ix, iy, iz); }
};

class InitCells : public OneCellIndexedFunctional3D<T,D> {
public:
    InitCells(bool isBeton_): isBeton(isBeton_) {}
    InitCells(InitCells const& rhs): isBeton(rhs.isBeton) {}
    InitCells* clone() const { return new InitCells(*this); }
    virtual void execute(plint ix, plint iy, plint iz, Cell<T,D>& cell) const {
        if ((!isBeton && isInWater(ix,iy,iz)) || (isBeton && isInBeton(ix,iy,iz))) {
            T h = max(nz - 2 - iz - inletH0, 0.0);
            T rho = 1 + D<T>::invCs2 * h * gravity * wRho / rho1;
            iniCellAtEquilibrium(cell, rho, Array<T,3>(0.,0.,0.));
        } else {
            iniCellAtEquilibrium(cell, 0.22, Array<T,3>(0.,0.,0.));
        }
    }
private:
    bool isBeton;
};

int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::directories().setOutputDir("./tmp/");
    
    makeDir("tmp");
    if (imageEvery > 0) {
        makeDir("tmp/i_beton_rho");
        makeDir("tmp/i_beton_ux");
        makeDir("tmp/i_beton_uz");
        makeDir("tmp/i_beton_omega");
        makeDir("tmp/i_beton_pineq");
        makeDir("tmp/i_water_rho");
        makeDir("tmp/i_water_ux");
        makeDir("tmp/i_water_uz");
    }
    makeDir("tmp/vtk");
    
    plb_ofstream pinfo("tmp/_info.txt");
    pcout.getOriginalStream().rdbuf(pinfo.getOriginalStream().rdbuf());
    
    const SparseBlockStructure3D basinStruct =
        createRegularDistribution3D(nx, ny, outletTop+1, basinBlocksX, basinBlocksY, 1);
    
    const SparseBlockStructure3D inletStruct =
        createRegularDistribution3D(
            Box3D (
                inletCX - inletR, inletCX + inletR + 1,
                inletCY - inletR, inletCY + inletR + 1,
                outletTop+1, nz-1
            ), 1, 1, inletBlocksZ);
    
    std::map<plint,std::vector<plint> > remappedIds;
    SparseBlockStructure3D struc = block_union(basinStruct, inletStruct, remappedIds);
    
    pcout << "basinStruct.getNumBulkCells=" << basinStruct.getNumBulkCells() << endl;
    pcout << "inletStruct.getNumBulkCells=" << inletStruct.getNumBulkCells() << endl;
    pcout << "struc.getNumBulkCells=" << struc.getNumBulkCells() << endl << endl;
    pcout << "basinStruct.getNumBlocks=" << basinStruct.getNumBlocks() << endl;
    pcout << "inletStruct.getNumBlocks=" << inletStruct.getNumBlocks() << endl;
    pcout << "struc.getNumBlocks=" << struc.getNumBlocks() << endl << endl;
    
    pcout << "rhoOutlet=" << rhoOutlet << endl;

    MultiBlockManagement3D betonManag
        (struc, defaultMultiBlockPolicy3D().getThreadAttribution(), envelopeWidth);
    
    MultiBlockLattice3D<T, D> betonLT(
        betonManag,
        defaultMultiBlockPolicy3D().getBlockCommunicator(),
        defaultMultiBlockPolicy3D().getCombinedStatistics(),
        defaultMultiBlockPolicy3D().getMultiCellAccess<T,D>(),
        new ExternalMomentRegularizedBGKdynamics<T, D>(1.0)
    );
    
    defineDynamics(betonLT, betonLT.getBoundingBox(), new IsInInlet, new GuoExternalForceBGKdynamics<T,D>(1.0));

    MultiBlockManagement3D waterManag
        (basinStruct, defaultMultiBlockPolicy3D().getThreadAttribution(), envelopeWidth);
    
    MultiBlockLattice3D<T, D> waterLT(
        waterManag,
        defaultMultiBlockPolicy3D().getBlockCommunicator(),
        defaultMultiBlockPolicy3D().getCombinedStatistics(),
        defaultMultiBlockPolicy3D().getMultiCellAccess<T,D>(),
        new ExternalMomentRegularizedBGKdynamics<T, D>(wOmega)
    );
    
    global::BinghamParameters().setOmegaMin(1. / 5.);
    global::BinghamParameters().setOmegaMax(1.25);
    global::BinghamParameters().setPlasticViscosity(plVisc);
    global::BinghamParameters().setYieldStress(yield);
    setCompositeDynamics(betonLT, betonLT.getBoundingBox(), new BinghamDynamics<T,D>(new NoDynamics<T,D>()));
    
    OnLatticeBoundaryCondition3D<T,D>* bc = createZouHeBoundaryCondition3D<T,D>();
      
    Box3D inletDomain(
        inletCX - inletR, inletCX + inletR + 1,
        inletCY - inletR, inletCY + inletR + 1,
        inletCZ, inletCZ);
    bc->addPressureBoundary2N(inletDomain, betonLT);
    setBoundaryDensity(betonLT, inletDomain, 1.0);
    
    Box3D outlet1Domain(
        outlet1CX - outlet1R, outlet1CX + outlet1R + 1,
        outlet1CY - outlet1R, outlet1CY + outlet1R + 1,
        outletCZ, outletCZ);
    bc->addPressureBoundary2N(outlet1Domain, waterLT);
    setBoundaryDensity(waterLT, outlet1Domain, rhoOutlet);
    
    Box3D outlet2Domain(
        outlet2CX - outlet2R, outlet2CX + outlet2R + 1,
        outlet2CY - outlet2R, outlet2CY + outlet2R + 1,
        outletCZ, outletCZ);
    bc->addPressureBoundary2N(outlet2Domain, waterLT);
    setBoundaryDensity(waterLT, outlet2Domain, rhoOutlet);
    
    Box3D outlet3Domain(
        outlet3CX - outlet3R, outlet3CX + outlet3R + 1,
        outlet3CY - outlet3R, outlet3CY + outlet3R + 1,
        outletCZ, outletCZ);
    bc->addPressureBoundary2N(outlet3Domain, waterLT);
    setBoundaryDensity(waterLT, outlet3Domain, rhoOutlet);
    
    Box3D outlet4Domain(
        outlet4CX - outlet4R, outlet4CX + outlet4R + 1,
        outlet4CY - outlet4R, outlet4CY + outlet4R + 1,
        outletCZ, outletCZ);
    bc->addPressureBoundary2N(outlet4Domain, waterLT);
    setBoundaryDensity(waterLT, outlet4Domain, rhoOutlet);
    
    vector<MultiBlockLattice3D<T, D>* > blockLattices;
    blockLattices.push_back(&betonLT);
    blockLattices.push_back(&waterLT);
    
    std::vector<T> constOmegaValues;
    constOmegaValues.push_back(1);
    constOmegaValues.push_back(1);
    integrateProcessingFunctional (
            new ShanChenMultiComponentProcessor3D<T,D>(1.0, constOmegaValues),
            Box3D(0,nx-1,0,ny-1,0,outletTop),
            blockLattices, 1 );
    
    setExternalVector(betonLT, betonLT.getBoundingBox(),
                      D<T>::ExternalField::forceBeginsAt, Array<T,3>(0.,0.,-gravity * bRho / rho1));
    setExternalVector(waterLT, waterLT.getBoundingBox(),
                      D<T>::ExternalField::forceBeginsAt, Array<T,3>(0.,0.,-0.3 * gravity * wRho / rho1));
    
    defineDynamics(waterLT, waterLT.getBoundingBox(), new IsInBBW, new BounceBack<T,D>);
    defineDynamics(betonLT, betonLT.getBoundingBox(), new IsInBB, new BounceBack<T,D>);
    
    applyIndexed(waterLT, waterLT.getBoundingBox(), new InitCells(false));
    applyIndexed(betonLT, betonLT.getBoundingBox(), new InitCells(true));
    
    waterLT.initialize();
    betonLT.initialize();

    plint cells = waterLT.getSparseBlockStructure().getNumBulkCells() + betonLT.getSparseBlockStructure().getNumBulkCells();
	
    plb_ofstream pumaxWC("tmp/umaxWC.txt");
    plb_ofstream pOmegaC("tmp/omegaC.txt");
    plb_ofstream pInflowRhoC("tmp/inflowRhoC.txt");
    plb_ofstream pInflowUzC("tmp/inflowUzC.txt");
    plb_ofstream pOutflow1RhoW("tmp/outflow1RhoW.txt");
    plb_ofstream pOutflow1RhoC("tmp/outflow1RhoC.txt");
    plb_ofstream pInOutUz("tmp/inOutUz.txt");
    
    for (int iT = 0; iT < maxIter; ++iT) {
        
        if (iT % infoEvery == 0) {
            pcout << "---" << iT << "---" << endl;
            double dur = global::timer("iteration").stop();
            global::timer("iteration").restart();
            pcout << "MFLOPS = " << infoEvery * cells / dur / 1e6 << " (" << dur << " sec)" << endl;
        }
        
        if(iT % statisticsEvery == 0) {
            Box3D lineIn(inletCX, inletCX, inletCY, inletCY, 0, nz-1);
            pInflowRhoC << *computeDensity(betonLT, lineIn) << endl;
            pInflowUzC << *computeVelocityComponent(betonLT, lineIn, 2) << endl;
            
            Box3D lineOut(outlet1CX, outlet1CX, outlet1CY, outlet1CY, 0, outletTop);
            pOutflow1RhoW << *computeDensity(waterLT, lineOut) << endl;
            pOutflow1RhoC << *computeDensity(betonLT, lineOut) << endl;
            
            MultiScalarField3D<T> omegas = *computeOmega(betonLT);
            omegaMin = 1e6; omegaSum = 0; omegaMax = -1e6; omegaN = 0;
            applyProcessingFunctional(new OmegaMinMeanMax, omegas.getBoundingBox(), omegas);
            pOmegaC << iT << "\t" << omegaMin << "\t" << omegaSum / omegaN << "\t" << omegaMax << endl;
            
            pumaxWC << iT << "\t" << computeMax(*computeVelocityNorm(waterLT)) << "\t" << computeMax(*computeVelocityNorm(betonLT)) << endl;
            
            pInOutUz << iT << "\t" <<
                getUz(betonLT, inletCX, inletCY, inletCZ) << "\t" <<
                getUz(waterLT, outlet1CX, outlet1CY, outletCZ) << "\t" << endl;
        }
        
        if (imageEvery > 0 && iT % imageEvery == 0) {
            Box3D slice(0, nx-1, ny/2, ny/2, 0, imageHeight);
            ImageWriter<T> imageWriter("leeloo");
            imageWriter.writeScaledPpm(createFileName("i_beton_rho/", iT, 6), *computeDensity(betonLT, slice));
            imageWriter.writeScaledPpm(createFileName("i_beton_ux/", iT, 6), *computeVelocityComponent(betonLT, slice, 0));
            imageWriter.writeScaledPpm(createFileName("i_beton_uz/", iT, 6), *computeVelocityComponent(betonLT, slice, 2));
            imageWriter.writeScaledPpm(createFileName("i_beton_omega/", iT, 6), *computeOmega(betonLT, slice));
            imageWriter.writeScaledPpm(createFileName("i_beton_pineq/", iT, 6), *computeSymmetricTensorNorm(*computePiNeq(betonLT, slice)));
            imageWriter.writeScaledPpm(createFileName("i_water_rho/", iT, 6), *computeDensity(waterLT, slice));
            imageWriter.writeScaledPpm(createFileName("i_water_ux/", iT, 6), *computeVelocityComponent(waterLT, slice, 0));
            imageWriter.writeScaledPpm(createFileName("i_water_uz/", iT, 6), *computeVelocityComponent(waterLT, slice, 2));
        }
        
        if(iT % vtkEvery == 0) {
            VtkImageOutput3D<T> vtkBetonTube(createFileName("vtk/betonTube", iT, 6), 1.);
            vtkBetonTube.writeData<float>(*computeDensity(betonLT, inletStruct.getBoundingBox()), "density", 1.);
            vtkBetonTube.writeData<float>(*computeOmega(betonLT, inletStruct.getBoundingBox()), "omega", 1.);
            vtkBetonTube.writeData<3,float>(*computeVelocity(betonLT, inletStruct.getBoundingBox()), "velocity", 1);
            
            VtkImageOutput3D<T> vtkBetonBasin(createFileName("vtk/betonBasin", iT, 6), 1.);
            vtkBetonBasin.writeData<float>(*computeDensity(betonLT, basinStruct.getBoundingBox()), "density", 1.);
            vtkBetonBasin.writeData<float>(*computeOmega(betonLT, basinStruct.getBoundingBox()), "omega", 1.);
            vtkBetonBasin.writeData<3,float>(*computeVelocity(betonLT, basinStruct.getBoundingBox()), "velocity", 1);

            VtkImageOutput3D<T> vtkWater(createFileName("vtk/water", iT, 6), 1.);
            vtkWater.writeData<float>(*computeDensity(waterLT), "density", 1.);
            vtkWater.writeData<3,float>(*computeVelocity(waterLT), "velocity", 1);
        }

        waterLT.collideAndStream();
        betonLT.collideAndStream();
    }
}

