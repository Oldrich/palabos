#include "palabos3D.h"
#include "palabos3D.hh"
#include <iostream>
#include "../../myLibs/headers3D.h"

using namespace plb;
using namespace std;

typedef double T;
#define D descriptors::ForcedD3Q19Descriptor

const plint maxIter  = 7e6;
const plint infoEvery = 50;
const plint statisticsEvery = 500;
const plint imageEvery = 500;
const plint vtkEvery = 5000;

const plint basinBlocksX = 6;
const plint basinBlocksY = 6;
const plint outletLBlocksZ = 1;

const T lb1m = 50;
const T lb1sec = 1000;

const T rhoC = 2300;
const T rhoW = 1000;
const T rhoRatio = rhoW / rhoC;

const T plVisc = 100.0 / rhoC * lb1m * lb1m / lb1sec;
const T yield = 100.0 / rhoC * lb1m * lb1m / lb1sec / lb1sec;

const T gravity = 9.8 * lb1m / lb1sec / lb1sec;

const T basinR = 2 * lb1m;
const T basinH = 0.25 * lb1m;

const T inletR = 0.15 * lb1m;
const T outletXR = 0.05 * lb1m;
const T outletLR = 0.25 * lb1m;

const T inletH0 = 23 * lb1m;
const T inletH1 = 23 * lb1m;
const T outletXH = 0.7 * lb1m;
const T outletLH = 1.4 * lb1m;

const plint inletCX = 0.5 + basinR + basinR * 1.5 / 4.0; // 0 degrees
const plint inletCY = 0.5 + basinR;

const plint outlet1CX = 0.5 + basinR + basinR * 3.5 / 4.0 * cos(1.0472); // 60 degrees
const plint outlet1CY = 0.5 + basinR + basinR * 3.5 / 4.0 * sin(1.0472);

const plint outlet2CX = 0.5 + basinR + basinR * 3.5 / 4.0 * cos(0.6981); // -40 degrees
const plint outlet2CY = 0.5 + basinR - basinR * 3.5 / 4.0 * sin(0.6981);

const plint outlet3CX = 0.5 + basinR - basinR * 3.5 / 4.0; // 180 degrees
const plint outlet3CY = 0.5 + basinR;

const plint outletLCX = 0.5 + basinR - basinR * 3.0 / 4.0 * cos(0.9425); // 180 + 54 degrees
const plint outletLCY = 0.5 + basinR - basinR * 3.0 / 4.0 * sin(0.9425);

const T outletLRho = 1.0 - outletLH * gravity * D<T>::invCs2 * rhoRatio;
const T outletXRho = 1.0 - outletXH * gravity * D<T>::invCs2 * rhoRatio;
const T inletRhoC0 = 1.0 + (inletH0 * (1.0 - rhoRatio) + inletH1) * gravity * D<T>::invCs2;

const T tauR = gravity * inletR / 2.0;
const T inletUz0Mean =
    //gravity * inletR * inletR / 3.0 / (1.5 * plVisc);
    inletR * tauR / 4.0 / plVisc * (1.0 - 4.0/3.0 * yield / tauR + 1.0 / 3.0 * pow(yield / tauR, 4));

const plint basinCZ = 0.5 + basinH + 1;

const plint outletXCZ = 0.5 + basinH + outletXH + 1;
const plint outletLCZ = 0.5 + basinH + outletLH + 1;
const plint inletCZ = basinCZ - 1;

const plint nx = ceil(2 * basinR) + 3;
const plint ny = ceil(2 * basinR) + 3;

T getUz(T rho) {
    return inletUz0Mean - rho * inletUz0Mean / inletRhoC0;
}

class SetUz : public BoxProcessingFunctional3D_L<T,D> {
public:
    virtual void process (Box3D d, BlockLattice3D<T,D>& lt) {
        for (plint iX=d.x0; iX<=d.x1; ++iX) {
            for (plint iY=d.y0; iY<=d.y1; ++iY) {
                for (plint iZ=d.z0; iZ<=d.z1; ++iZ) {
                    Cell<T,D>& cell = lt.get(iX,iY,iZ);
                    T rho = cell.computeDensity();
                    Array<T,3> u;
                    cell.computeVelocity(u);
                    T uz = 0.9 * u[2] - 0.1 * getUz(rho);
                    cell.defineVelocity(Array<T,3>(0,0,uz));
                }
            }
        }
    }
    virtual SetUz* clone() const { return new SetUz(*this); }
    virtual BlockDomain::DomainT appliesTo() const { return BlockDomain::bulkAndEnvelope; }
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const { modified[0] = modif::staticVariables; }
};

bool isInBB(plint ix, plint iy, plint iz) {
    const T e = 1e-5;
    const T br = sqrt(pow(ix - 0.5 - basinR, 2) + pow(iy - 0.5 - basinR, 2));
    const T irxy = sqrt(pow(ix - basinR, 2) + pow(iy - basinR, 2));
    const T iryz = sqrt(pow(iy - inletCY, 2) + pow(iz - (basinCZ - 0.1 * lb1m), 2));
    const T or1 = sqrt(pow(ix - outlet1CX, 2) + pow(iy - outlet1CY, 2));
    const T or2 = sqrt(pow(ix - outlet2CX, 2) + pow(iy - outlet2CY, 2));
    const T or3 = sqrt(pow(ix - outlet3CX, 2) + pow(iy - outlet3CY, 2));
    const T orL = sqrt(pow(ix - outletLCX, 2) + pow(iy - outletLCY, 2));
    return
        iz == 0 ||
        br > basinR + e ||
        (
            iz >= basinCZ && (irxy > inletR + e || iz > basinCZ) &&
            (or1 > outletXR + e || iz > outletXCZ) && (or2 > outletXR + e || iz > outletXCZ) &&
            (or3 > outletXR + e || iz > outletXCZ) && (orL > outletLR + e || iz > outletLCZ)
        ) ||
        ( iryz < 0.05 * lb1m && ix > basinR + inletR + 2 && ix < inletCX + inletR );
}

int initialFluidFlags(plint ix, plint iy, plint iz) {
    const T ir = sqrt(pow(ix - basinR, 2) + pow(iy - basinR, 2));
    if (isInBB(ix,iy,iz)) { return twoPhaseFlag::wall; }
    else if (iz == basinCZ && ir < inletR + 1e-10) { return twoPhaseFlag::fluid; }
    else { return twoPhaseFlag::empty; }
}

int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::directories().setOutputDir("./tmp/");
    
    makeDir("tmp");
    if (imageEvery > 0) {
        makeDir("tmp/i_pressure");
        makeDir("tmp/i_ux");
        makeDir("tmp/i_uz");
        makeDir("tmp/i_omega");
        makeDir("tmp/i_pineq");
    }
    makeDir("tmp/vtk");
    
    plb_ofstream pinfo("tmp/_info.txt");
    pcout.getOriginalStream().rdbuf(pinfo.getOriginalStream().rdbuf());
    
    SparseBlockStructure3D basinStruct =
        createRegularDistribution3D(nx, ny, basinCZ+2, basinBlocksX, basinBlocksY, 1);
    
    const Box3D basinDomain = basinStruct.getBoundingBox();
    
    const Box3D tube1Domain(
        floor(outlet1CX - outletXR) - 1, ceil(outlet1CX + outletXR) + 1,
        floor(outlet1CY - outletXR) - 1, ceil(outlet1CY + outletXR) + 1,
        basinCZ+2, outletXCZ + 1);
    basinStruct.addBlock(tube1Domain, basinStruct.nextIncrementalId());
    
    const Box3D tube2Domain(
        floor(outlet2CX - outletXR) - 1, ceil(outlet2CX + outletXR) + 1,
        floor(outlet2CY - outletXR) - 1, ceil(outlet2CY + outletXR) + 1,
        basinCZ+2, outletXCZ + 1);
    basinStruct.addBlock(tube2Domain, basinStruct.nextIncrementalId());
    
    const Box3D tube3Domain(
        floor(outlet3CX - outletXR) - 1, ceil(outlet3CX + outletXR) + 1,
        floor(outlet3CY - outletXR) - 1, ceil(outlet3CY + outletXR) + 1,
        basinCZ+2, outletXCZ + 1);
    basinStruct.addBlock(tube3Domain, basinStruct.nextIncrementalId());
    
    const Box3D tubeLDomain(
            floor(outletLCX - outletLR) - 1, ceil(outletLCX + outletLR) + 1,
            floor(outletLCY - outletLR) - 1, ceil(outletLCY + outletLR) + 1,
            basinCZ+2, outletLCZ + 1);
    
    SparseBlockStructure3D tubeLStruct(tubeLDomain, 1, 1, outletLBlocksZ);
    tubeLStruct.addBlock(tubeLDomain, tubeLStruct.nextIncrementalId());
    
    std::map<plint,std::vector<plint> > remappedIds;
    const SparseBlockStructure3D struc = block_union(basinStruct, tubeLStruct, remappedIds);
    
    pcout << "struct.getNumBlocks=" << struc.getNumBlocks() << endl;
    pcout << "struct.getNumBulkCells=" << struc.getNumBulkCells() << endl;

    pcout << "inletRhoC0=" << inletRhoC0 << endl;
    pcout << "rhoOutletL=" << outletLRho << endl;
    pcout << "rhoOutletX=" << outletXRho << endl;
    pcout << "inletUz0Mean=" << inletUz0Mean << endl;
    
    Bingham::omegaMin =  1. / 2.;
    Bingham::plasticViscosity = plVisc;
    Bingham::yieldStress = yield;

    TwoPhaseFields3D<T,D> fs(
        struc,
        new Bingham::BinghamDynamics<T,D>(new RegularizedBGKdynamics<T,D>(1.0)),
        //new RegularizedBGKdynamics<T,D>(1.0),
        new RegularizedBGKdynamics<T, D>(1.0),
        1, rhoRatio, 0, -1, Array<T,3>(0.,0.,-gravity),
        Array<T,3>(0.,0.,-gravity * rhoRatio), dynamic );
    fs.periodicityToggleAll(false);
    setToFunction(fs.flag, fs.flag.getBoundingBox(), initialFluidFlags);
    fs.partiallyDefaultInitialize();
    
    const plint nz = fs.lattice.getNz();
    
    OnLatticeBoundaryCondition3D<T,D>* bc = createZouHeBoundaryCondition3D<T,D>();
      
    const Box3D inletDomain(
        basinR - inletR, basinR + inletR + 1,
        basinR - inletR, basinR + inletR + 1,
        basinCZ, basinCZ);
    const Box3D outlet1Domain(
        outlet1CX - outletXR, outlet1CX + outletXR + 1,
        outlet1CY - outletXR, outlet1CY + outletXR + 1,
        outletXCZ, outletXCZ);
    const Box3D outlet2Domain(
        outlet2CX - outletXR, outlet2CX + outletXR + 1,
        outlet2CY - outletXR, outlet2CY + outletXR + 1,
        outletXCZ, outletXCZ);
    const Box3D outlet3Domain(
        outlet3CX - outletXR, outlet3CX + outletXR + 1,
        outlet3CY - outletXR, outlet3CY + outletXR + 1,
        outletXCZ, outletXCZ);
    const Box3D outletLDomain(
        outletLCX - outletLR, outletLCX + outletLR + 1,
        outletLCY - outletLR, outletLCY + outletLR + 1,
        outletLCZ, outletLCZ);
    
    bc->addVelocityBoundary2P(inletDomain, fs.lattice);
    bc->addPressureBoundary2P(outlet1Domain, *fs.lattice2);
    bc->addPressureBoundary2P(outlet2Domain, *fs.lattice2);
    bc->addPressureBoundary2P(outlet3Domain, *fs.lattice2);
    bc->addPressureBoundary2P(outletLDomain, *fs.lattice2);
    
    setBoundaryDensity(fs.lattice, inletDomain, 1.0);
    setBoundaryDensity(*fs.lattice2, outlet1Domain, outletXRho);
    setBoundaryDensity(*fs.lattice2, outlet2Domain, outletXRho);
    setBoundaryDensity(*fs.lattice2, outlet3Domain, outletXRho);
    setBoundaryDensity(*fs.lattice2, outletLDomain, outletLRho);
    
    integrateProcessingFunctional(new SetUz, fs.lattice.getBoundingBox(), fs.lattice, 0);
    
    const plint cells =
        fs.lattice.getSparseBlockStructure().getNumBulkCells() +
        fs.lattice2->getSparseBlockStructure().getNumBulkCells();
	
    plb_ofstream pumax("tmp/umax.txt");
    plb_ofstream pmass("tmp/mass.txt");
    plb_ofstream prho("tmp/rho.txt");
    plb_ofstream plostMass("tmp/lostMass.txt");
    
    plb_ofstream pInflowRho("tmp/inflowRho.txt");
    plb_ofstream pInflowUz("tmp/inflowUz.txt");
    
    plb_ofstream pOutLRho("tmp/outLRho.txt");
    plb_ofstream pOutLUx("tmp/outUx.txt");
    plb_ofstream pOutLUy("tmp/outUy.txt");
    plb_ofstream pOutLUz("tmp/outUz.txt");
    
    for (int iT = 0; iT < maxIter; ++iT) {
        
        if (iT % infoEvery == 0) {
            pcout << "---" << iT << "---" << endl;
            const double dur = global::timer("iteration").stop();
            global::timer("iteration").restart();
            pcout << "MFLOPS = " << infoEvery * cells / dur / 1e6 << " (" << dur << " sec)" << endl;
        }
        
        if( statisticsEvery > 0 && (
            iT % statisticsEvery == 0 ||
            (iT - 1) % statisticsEvery == 0 ||
            (iT + 1) % statisticsEvery == 0 )) {
            const Box3D lineIn(inletCX, inletCX, inletCY, inletCY, 0, inletCZ + 1);
            pInflowRho << *computeDensity(fs.lattice, lineIn) << endl;
            pInflowUz << *computeVelocityComponent(fs.lattice, lineIn, 2) << endl;
            
            const Box3D lineOutL(outletLCX, outletLCX, outletLCY, outletLCY, 0, outletLCZ + 1);
            pOutLRho << *computeDensity(*fs.lattice2, lineOutL) << endl;
            pOutLUx << *computeVelocityComponent(*fs.lattice2, lineOutL, 0) << endl;
            pOutLUy << *computeVelocityComponent(*fs.lattice2, lineOutL, 1) << endl;
            pOutLUz << *computeVelocityComponent(*fs.lattice2, lineOutL, 2) << endl;
            
            pumax << iT << "\t" << computeMax(*computeNorm(*fs.computeVelocity())) << endl;
            pmass << iT << "\t" << computeSum(fs.volumeFraction) << endl;
            prho << iT << "\t" << computeSum(*computeDensity(fs.lattice)) << endl;
            plostMass << iT << "\t" << fs.lattice.getInternalStatistics().getSum(1) << endl;
        }
        
        if ( imageEvery > 0 && (
            iT % imageEvery == 0 ||
            (iT - 1) % imageEvery == 0 ||
            (iT + 1) % imageEvery == 0 )) {
            const Box3D slice(0, nx-1, ny/2, ny/2, 0, nz-1);
            ImageWriter<T> imageWriter("leeloo");
            imageWriter.writeScaledPpm(createFileName("i_pressure/", iT, 6), *fs.computePressure(slice));
            imageWriter.writePpm(createFileName("i_ux/", iT, 6), *extractComponent(*fs.computeVelocity(slice), 0), -5*inletUz0Mean, 5*inletUz0Mean);
            imageWriter.writePpm(createFileName("i_uz/", iT, 6), *extractComponent(*fs.computeVelocity(slice), 2), -5*inletUz0Mean, 5*inletUz0Mean);
            imageWriter.writePpm(createFileName("i_omega/", iT, 6), *computeOmega(fs.lattice, slice), Bingham::omegaMin, 1.0 / (plVisc * 3 + 0.5));
            imageWriter.writeScaledPpm(createFileName("i_pineq/", iT, 6), *computeSymmetricTensorNorm(*computePiNeq(fs.lattice, slice)));
        }

        if(iT % vtkEvery == 0) {
            std::auto_ptr<MultiScalarField3D<T> > fl = copyConvert<int, T>(fs.flag);
            
            {
            global::mpi().barrier();
            VtkImageCellOutput3D<T> vtkBasin(createFileName("vtk/basin", iT, 6), 1.);
            vtkBasin.writeData<float>(*fs.computePressure(basinDomain), "pressure", 1.);
            global::mpi().barrier();
            vtkBasin.writeData<float>(*computeDensity(fs.lattice, basinDomain), "rhoC", 1.);
            global::mpi().barrier();
            vtkBasin.writeData<float>(*computeDensity(*fs.lattice2, basinDomain), "rhoW", 1.);
            global::mpi().barrier();
            vtkBasin.writeData<float>(*computeOmega(fs.lattice, basinDomain), "omega", 1.);
            global::mpi().barrier();
            vtkBasin.writeData<3,float>(*fs.computeVelocity(basinDomain), "velocity", 1);
            global::mpi().barrier();
            vtkBasin.writeData<float>(*extractSubDomain(*add(*fl, fs.volumeFraction), basinDomain), "kind", 1.);
            global::mpi().barrier();
            }
            
            {
            VtkImageCellOutput3D<T> vtkOutlet1(createFileName("vtk/outletA", iT, 6), 1.);
            vtkOutlet1.writeData<float>(*fs.computePressure(tube1Domain), "pressure", 1.);
            vtkOutlet1.writeData<float>(*computeDensity(fs.lattice, tube1Domain), "rhoC", 1.);
            vtkOutlet1.writeData<float>(*computeDensity(*fs.lattice2, tube1Domain), "rhoW", 1.);
            vtkOutlet1.writeData<float>(*computeOmega(fs.lattice, tube1Domain), "omega", 1.);
            vtkOutlet1.writeData<3,float>(*fs.computeVelocity(tube1Domain), "velocity", 1);
            vtkOutlet1.writeData<float>(*extractSubDomain(*add(*fl, fs.volumeFraction), tube1Domain), "kind", 1.);
            }
            
            {
            VtkImageCellOutput3D<T> vtkOutlet2(createFileName("vtk/outletB", iT, 6), 1.);
            vtkOutlet2.writeData<float>(*fs.computePressure(tube2Domain), "pressure", 1.);
            vtkOutlet2.writeData<float>(*computeDensity(fs.lattice, tube2Domain), "rhoC", 1.);
            vtkOutlet2.writeData<float>(*computeDensity(*fs.lattice2, tube2Domain), "rhoW", 1.);
            vtkOutlet2.writeData<float>(*computeOmega(fs.lattice, tube2Domain), "omega", 1.);
            vtkOutlet2.writeData<3,float>(*fs.computeVelocity(tube2Domain), "velocity", 1);
            vtkOutlet2.writeData<float>(*extractSubDomain(*add(*fl, fs.volumeFraction), tube2Domain), "kind", 1.);
            }
            
            {
            VtkImageCellOutput3D<T> vtkOutlet3(createFileName("vtk/outletC", iT, 6), 1.);
            vtkOutlet3.writeData<float>(*fs.computePressure(tube3Domain), "pressure", 1.);
            vtkOutlet3.writeData<float>(*computeDensity(fs.lattice, tube3Domain), "rhoC", 1.);
            vtkOutlet3.writeData<float>(*computeDensity(*fs.lattice2, tube3Domain), "rhoW", 1.);
            vtkOutlet3.writeData<float>(*computeOmega(fs.lattice, tube3Domain), "omega", 1.);
            vtkOutlet3.writeData<3,float>(*fs.computeVelocity(tube3Domain), "velocity", 1);
            vtkOutlet3.writeData<float>(*extractSubDomain(*add(*fl, fs.volumeFraction), tube3Domain), "kind", 1.);
            }
            
            {
            VtkImageCellOutput3D<T> vtkOutletL(createFileName("vtk/outletL", iT, 6), 1.);
            vtkOutletL.writeData<float>(*fs.computePressure(tubeLDomain), "pressure", 1.);
            vtkOutletL.writeData<float>(*computeDensity(fs.lattice, tubeLDomain), "rhoC", 1.);
            vtkOutletL.writeData<float>(*computeDensity(*fs.lattice2, tubeLDomain), "rhoW", 1.);
            vtkOutletL.writeData<float>(*computeOmega(fs.lattice, tubeLDomain), "omega", 1.);
            vtkOutletL.writeData<3,float>(*fs.computeVelocity(tubeLDomain), "velocity", 1);
            vtkOutletL.writeData<float>(*extractSubDomain(*add(*fl, fs.volumeFraction), tubeLDomain), "kind", 1.);
            }
            
        }

        fs.lattice2->executeInternalProcessors();

        fs.lattice.executeInternalProcessors();

        fs.lattice.evaluateStatistics();
        fs.lattice.incrementTime();

        fs.lattice2->evaluateStatistics();
        fs.lattice2->incrementTime();
    }
    
    delete bc;
    return 0;
}