#include "palabos3D.h"
#include "palabos3D.hh"
#include <cstdlib>
#include <iostream>

using namespace plb;
using namespace std;

typedef double T;
#define D descriptors::ForcedShanChenD3Q19Descriptor


const T lb1m = 25;
const T lb1sec = 1000;
const T rho1 = 1000;

const T wRho = 1000; // kg/m3
const T bRho = 2300; // kg/m3

const T plVisc = 50.0 / bRho * lb1m * lb1m / lb1sec;
const T yield = 50.0 / bRho * lb1m * lb1m / lb1sec / lb1sec;

const T wVisc = 0.1;
const T wOmega = 1.0 / (wVisc * D<T>::invCs2 + 0.5);


const T G = 1.2;


const T inletR = 0.5 * lb1m; // m
const T inletCX = 0.5 * lb1m; // m
const T inletCY = 0.5 * lb1m; // m
const T inletH0 = 0 * lb1m; // m
const T inletH1 = 0.5 * lb1m; // m
const T basinR = 0.5 * lb1m; // m
const T basinH = 0.5 * lb1m; // m
const T outlet1CX = 0.2 * lb1m; // m
const T outlet1CY = 0.5 * lb1m; // m
const T outlet1R = 0.1 * lb1m; // m
const T outletH = 0.1 * lb1m; //m

const T gravity = 9.8 * lb1m / lb1sec / lb1sec; // m/s2

const T inletCXi = ceil(inletCX) - 0.5;
const T inletCYi = ceil(inletCY) - 0.5;

const plint nx = ceil(2 * basinR) + 2;
const plint nz = ceil(basinH + inletH0 + inletH1) + 2;

bool isInInlet(plint ix, plint iy, plint iz) {
    T rForInlet = sqrt(pow(ix - inletCXi, 2) + pow(iy - inletCYi, 2));
    return iz > basinH + 1 && rForInlet < inletR + 1e-3;
}

bool isInBasin(plint ix, plint iy, plint iz) {
    T rForBasin = sqrt(pow(ix - basinR - 0.5, 2) + pow(iy - basinR - 0.5, 2));
    return iz > 0 && iz <= basinH + 1 && rForBasin < basinR;
}

bool isInOutlet1(plint ix, plint iy, plint iz) {
    T rForOutlet1 = sqrt(pow(ix - outlet1CX - 0.5, 2) + pow(iy - outlet1CY - 0.5, 2));
    return false;// iz > basinH + 1 && iz < basinH + 1 + outletH && rForOutlet1 < outlet1R + 1e-3; 
}

bool isInBB(plint ix, plint iy, plint iz) {
    return !isInInlet(ix,iy,iz) && !isInBasin(ix,iy,iz) && !isInOutlet1(ix,iy,iz);
}

bool isInWater(plint ix, plint iy, plint iz) {
    return isInBasin(ix,iy,iz) || isInOutlet1(ix,iy,iz);
}

bool isInBeton(plint ix, plint iy, plint iz) {
    return isInInlet(ix,iy,iz);
}

class IsInInlet : public DomainFunctional3D {
public:
    IsInInlet(){}
    IsInInlet* clone() const { return new IsInInlet(*this); }
    virtual bool operator() (plint ix, plint iy, plint iz) const {
        return isInInlet(ix, iy, iz);
    }
};

class IsInBB : public DomainFunctional3D {
public:
    IsInBB(bool isBeton_): isBeton(isBeton_) {}
    IsInBB* clone() const { return new IsInBB(*this); }
    virtual bool operator() (plint ix, plint iy, plint iz) const {
        return isInBB(ix, iy, iz);
    }
private:
    bool isBeton;
};

class InitCells : public OneCellIndexedFunctional3D<T,D> {
public:
    InitCells(bool isBeton_): isBeton(isBeton_) {}
    InitCells* clone() const { return new InitCells(*this); }
    virtual void execute(plint ix, plint iy, plint iz, Cell<T,D>& cell) const {
        if ((!isBeton && isInWater(ix,iy,iz)) || (isBeton && isInBeton(ix,iy,iz))) {
            T h = max(nz - 1.0 - iz - inletH0, 0.0);
            T rho = 1.0;//0.95 * (1 + 3 * h * gravity * wRho / rho1);
            iniCellAtEquilibrium(cell, rho, Array<T,3>(0.,0.,0.));
        } else {
            iniCellAtEquilibrium(cell, 0.1, Array<T,3>(0.,0.,0.));
        }
    }
private:
    bool isBeton;
};



int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");
    
    pcout << "max u = " << gravity / 2 / wVisc * inletR * inletR << endl;
    
    
    
    
//    BinghamDynamics<T,D>* bDynamics2 = new BinghamDynamics<T,D>(new NoDynamics<T,D>());
//    bDynamics2->omegaMin = 1. / 5.;
//    bDynamics2->plasticViscosity = plVisc;
//    bDynamics2->yieldStress = yield;
//    setCompositeDynamics(betonLT, betonLT.getBoundingBox(), bDynamics2);
    
    
    
    defineDynamics(waterLT, waterLT.getBoundingBox(), new IsInBB(false), new BounceBack<T,D>);
    defineDynamics(betonLT, betonLT.getBoundingBox(), new IsInBB(true), new BounceBack<T,D>);
    
    applyIndexed(waterLT, waterLT.getBoundingBox(), new InitCells(false));
    applyIndexed(betonLT, betonLT.getBoundingBox(), new InitCells(true));
    
    setExternalVector(betonLT, betonLT.getBoundingBox(),
                      D<T>::ExternalField::forceBeginsAt, Array<T,3>(0,0,-gravity * bRho / rho1));
    
    setExternalVector(waterLT, waterLT.getBoundingBox(),
                      D<T>::ExternalField::forceBeginsAt, Array<T,3>(0,0,-gravity * wRho / rho1));
    
    MultiScalarField3D<int> betonFlags(betonLT);
    setToFunction(betonFlags, betonFlags.getBoundingBox(), isInBeton);
    
    MultiScalarField3D<int> waterFlags(waterLT);
    setToFunction(waterFlags, waterFlags.getBoundingBox(), isInWater);
    
    plint betonCells = computeSum(betonFlags);
    plint waterCells = computeSum(waterFlags);
    
    
    
    betonLT.initialize();
    waterLT.initialize();
    
    
    
    for (int iT=0; iT < 1000; ++iT) {
        
        
        betonLT.collideAndStream();
        waterLT.collideAndStream();
        
        global::timer("iteration").restart();
        
    }
    return 0;
}
