#include "palabos3D.h"
#include "palabos3D.hh"
#include "../../myLibs/headers3D.h"
#include "liftCone3D.h"

using namespace plb;
using namespace std;

#define D descriptors::ForcedD3Q19Descriptor
typedef double T;

int main(int argc, char **argv) {
    
plbInit(&argc, &argv);
for (int i = 0; i < argc; i++){ pcout << argv[i] << " "; }; pcout << endl;

makeDir("tmp");
global::directories().setOutputDir("tmp/");

plb_ofstream pinfo("tmp/_info.txt");
pcout.getOriginalStream().rdbuf(pinfo.getOriginalStream().rdbuf());

T plVisc = 0.1;
T plViscReal = 60. / 2500.;
T yieldReal = 30. / 2500.;
T lb1m = 200;
T liftSpeedReal = 0.07;

if (global::argc() > 1) {
    global::argv(1).read(plVisc);
    global::argv(2).read(plViscReal);
    global::argv(3).read(yieldReal);
    global::argv(4).read(lb1m);
    global::argv(5).read(liftSpeedReal);
}

const T lb1sec = plViscReal / plVisc * lb1m * lb1m;

const plint maxIter  = 20.0 * lb1sec;
const plint infoEvery = 10;
const plint statisticsEvery = 50;
const plint imageEvery = 50;
const plint vtkEvery = 100;

if (imageEvery > 0) {
    makeDir("tmp/i_volFr");
    makeDir("tmp/i_mass");
    makeDir("tmp/i_rhoBar");
    makeDir("tmp/i_ux");
    makeDir("tmp/i_uz");
    makeDir("tmp/i_omega");
    makeDir("tmp/i_pineq");
}
if (vtkEvery > 0) {
    makeDir("tmp/vtk");
}

const T yield = yieldReal * lb1m * lb1m / lb1sec / lb1sec;
const T gravity = 9.8 * lb1m / lb1sec / lb1sec;
const plint liftPeriod = 1.0 / (liftSpeedReal * lb1m / lb1sec);

const T lx = 0.8 * lb1m;
const T lz = 0.35 * lb1m;

const plint nx = util::roundToInt(lx);
const plint nz = util::roundToInt(lz);

pcout << "lb1sec = " << lb1sec << endl;
pcout << "liftPeriod = " << liftPeriod << endl;

auto initCells = [lx, lb1m] (plint iX, plint iY, plint iZ) {
    T coneBottomDiameter = 0.2 * lb1m;
    T coneTopDiameter = 0.1 * lb1m;
    T coneHeight = 0.3 * lb1m;
    T dxx = 0.5 * (coneBottomDiameter - coneTopDiameter);
    T r = 0.5 * coneBottomDiameter - iZ / coneHeight * dxx;
    T dx = iX - 0.5 * lx;
    T dy = iY - 0.5 * lx;
    if (dx * dx + dy * dy < r * r && iZ < coneHeight) {
        return twoPhaseFlag::fluid;
//    } else if (dx * dx + dy * dy < (r + 3) * (r + 3) && iZ < coneHeight) {
//        return twoPhaseFlag::wall;
    } else {
        return twoPhaseFlag::empty;
    }
};

const plint nxmin = 0.3*nx, nxmax = 0.7*nx, nzmid = 0.2*nz;
SparseBlockStructure3D struc(Box3D(0,nx-1, 0,nx-1, 0,nz-1), 1,1,1);
struc.addBlock(Box3D(0,nx-1, 0,nx-1, 0,nzmid-1), struc.nextIncrementalId());
struc.addBlock(Box3D(nxmin,nxmax, nxmin,nxmax, nzmid,nz-1), struc.nextIncrementalId());

Bingham::omegaMin =  1. / 5.;
Bingham::plasticViscosity = plVisc;
Bingham::yieldStress = yield;

FreeSurfaceFields3D<T,D> fs(
    struc,
    //new BGKdynamics<T,D>(1.0),
    new Bingham::BinghamDynamics<T,D>(new BGKdynamics<T,D>(1.0)),
    1, 0, -1, Array<T,3>(0.,0.,-gravity));
fs.periodicityToggleAll(false);
setToFunction(fs.flag, fs.flag.getBoundingBox(), initCells);
fs.defaultInitialize();
fs.lattice.internalStatSubscription().subscribeAverage();

//integrateBinghamFunctional<T,D>(plVisc, yield, 1./5., fs);

const plint cells = fs.lattice.getSparseBlockStructure().getNumBulkCells();

plb_ofstream pspread("tmp/spread.txt");
plb_ofstream plostMass("tmp/lostMass.txt");
plb_ofstream pumax("tmp/umax.txt");
plb_ofstream pOmega("tmp/omega.txt");
plb_ofstream pOmegaStat("tmp/omegaStat.txt");
plb_ofstream pPiNeq("tmp/pineq.txt");

for (plint iT = 0; iT <= maxIter; ++iT) {

    if (iT % infoEvery == 0) {
        pcout << "---" << iT << "---" << endl;
        const double dur = global::timer("iteration").stop();
        global::timer("iteration").restart();
        pcout << "MFLOPS = " << infoEvery * cells / dur / 1e6 << " (" << dur << " sec)" << endl;
    }

    if(statisticsEvery > 0 && iT % statisticsEvery == 0) {
        const Box3D line(lx/2., lx/2., lx/2., lx/2., 0, nz-1);
        pOmega << *computeOmega(fs.lattice, line) << endl;
        pPiNeq << *computeSymmetricTensorNorm(*computePiNeq(fs.lattice, line)) << endl;
        pumax << iT << "\t" << computeMax(*computeVelocityNorm(fs.lattice)) << endl;
        plostMass << iT << "\t" << fs.lattice.getInternalStatistics().getSum(1) << endl;
        pOmegaStat << iT << "\t" << computeAverage(*computeOmega(fs.lattice),  fs.flag, 2) << endl;

        const Box3D slice(0, nx-1, 0, nx-1, 1, 1);
        const T sumFraction = computeSum(*extractSubDomain(fs.volumeFraction, slice));
        const T spread = 2.0 * sqrt(sumFraction / 3.14159265359) / lb1m;
        pspread << iT / lb1sec << "\t" << spread << endl;
    }

    if (imageEvery > 0 && iT % imageEvery == 0) {
        const Box3D slice(0, nx-1, nx/2, nx/2, 0, nz-1);
        ImageWriter<T> imageWriter("leeloo");
        imageWriter.writeScaledPpm(createFileName("i_volFr/", iT, 6), *extractSubDomain(fs.volumeFraction, slice));
        imageWriter.writeScaledPpm(createFileName("i_mass/", iT, 6), *extractSubDomain(fs.mass, slice));
        imageWriter.writeScaledPpm(createFileName("i_rhoBar/", iT, 6), *extractSubDomain(fs.rhoBar, slice));
        imageWriter.writeScaledPpm(createFileName("i_ux/", iT, 6), *computeVelocityComponent(fs.lattice, slice, 0));
        imageWriter.writeScaledPpm(createFileName("i_uz/", iT, 6), *computeVelocityComponent(fs.lattice, slice, 2));
        imageWriter.writeScaledPpm(createFileName("i_omega/", iT, 6), *computeOmega(fs.lattice, slice));
        imageWriter.writeScaledPpm(createFileName("i_pineq/", iT, 6), *computeSymmetricTensorNorm(*computePiNeq(fs.lattice, slice)));
    }

    if(vtkEvery > 0 && iT % vtkEvery == 0) {
        VtkImageCellOutput3D<T> vtk(createFileName("vtk/fluid", iT, 6), 1.);
        vtk.writeData<float>(fs.rhoBar, "rho", 1.);
        vtk.writeData<float>(*computeOmega(fs.lattice), "omega", 1.);
        vtk.writeData<3,float>(*computeVelocity(fs.lattice), "velocity", 1);
        vtk.writeData<float>(*add(*copyConvert<int, T>(fs.flag), fs.volumeFraction), "kind", 1.);
    }

    fs.lattice.executeInternalProcessors();

    if (iT > 0 && iT % liftPeriod == 0) {
        liftCone3D(fs);
    }

    fs.lattice.evaluateStatistics();
    fs.lattice.incrementTime();
}

return 0;

}
