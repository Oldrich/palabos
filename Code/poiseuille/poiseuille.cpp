#include "palabos2D.h"
#include "palabos2D.hh"

using namespace plb;
using namespace plb::descriptors;
using namespace std;

typedef double T;
#define DESCRIPTOR ForcedD2Q9Descriptor

//PLB_DEBUG

void writeGifs(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter)
{
    const plint imSize = 600;

    ImageWriter<T> imageWriter("leeloo");
    imageWriter.writeScaledGif(createFileName("u", iter, 6),
                               *computeVelocityNorm(lattice),
                               imSize, imSize );
}

int main(int argc, char* argv[]) {
    plbInit(&argc, &argv);
    global::directories().setOutputDir("./tmp/");
    
    plb_ofstream pinfo("tmp\\_info.txt");
    plb_ofstream pshape("tmp\\_shape.txt");
    
    T lb1m = 100;
    T lb1sec = 16000;
    
    T nu = 0.02;
    T yield = 0.04;
    T g = 1;
    T nx = 0.6;
    T ny = 0.2;

    T nuLB = nu * lb1m * lb1m / lb1sec;
    T yieldLB = yield * lb1m * lb1m / lb1sec / lb1sec;  //3e-4; //2e-4;
    T gLB = g * lb1m / lb1sec / lb1sec; // 6e-5;
    plint nxLB = nx * lb1m;
    plint nyLB = ny * lb1m + 2;
        
    MultiBlockLattice2D<T, DESCRIPTOR> lattice(nxLB, nyLB, new GuoExternalForceBGKdynamics<T,DESCRIPTOR>(1) );
    
    lattice.periodicity().toggleAll(true);
    
    BinghamDynamics<T,DESCRIPTOR>* pbDyn = new BinghamDynamics<T,DESCRIPTOR>(new NoDynamics<T,DESCRIPTOR>());
    pbDyn->omegaMin = 1.0 / 2.0;
    pbDyn->plasticViscosity = nuLB;
    pbDyn->yieldStress = yieldLB;
    setCompositeDynamics(lattice, lattice.getBoundingBox(), pbDyn);
 
    defineDynamics(lattice, Box2D(0, nxLB - 1, 0, 0), new BounceBack<T, DESCRIPTOR>());
    defineDynamics(lattice, Box2D(0, nxLB - 1, nyLB - 1, nyLB - 1), new BounceBack<T, DESCRIPTOR>());
    
    setExternalVector(lattice, lattice.getBoundingBox(), DESCRIPTOR<T>::ExternalField::forceBeginsAt, Array<T,2>(gLB, 0));
    
    initializeAtEquilibrium(lattice,lattice.getBoundingBox(),1.0,Array<T, 2>(0,0));
    
    lattice.initialize();
    
    for (plint iT=0; iT < 50000; ++iT) {
        
        if (iT % 1000 == 0) {
            pinfo << "Saving Gif ..." << endl;
            writeGifs(lattice, iT);
            plint nx2 = nxLB / 2;
            Box2D slice(nx2, nx2, 0, nyLB - 1);
            pshape << *multiply(*computeVelocityNorm(lattice, slice), lb1sec / lb1m) << endl;
        }

        if (iT % 100 == 0) {
            pinfo << "step " << iT << "; lattice time=" << lattice.getTimeCounter().getTime() << endl;
        }

        lattice.collideAndStream();

        if (iT % 100 == 0) {
            pinfo << "; av energy="
                  << setprecision(10) << getStoredAverageEnergy<T>(lattice)
                  << "; av rho="
                  << getStoredAverageDensity<T>(lattice) << endl;
        }
    }

}
