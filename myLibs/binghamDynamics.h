#ifndef BINGHAM_DYNAMICS_H
#define BINGHAM_DYNAMICS_H

namespace plb {
namespace Bingham {
    
double yieldStress;
double plasticViscosity;
double omegaMin;

template<typename T, template<typename U> class D>
class BinghamDynamics : public VariableOmegaDynamics<T,D> {
public:
    BinghamDynamics(Dynamics<T,D>* baseDynamics, bool automaticPrepareCollision = true)
    : VariableOmegaDynamics<T,D>(baseDynamics, automaticPrepareCollision){}
    
    BinghamDynamics<T,D>* clone() const { return new BinghamDynamics<T,D>(*this); }
    
    virtual int getId() const { return id; }
    
    virtual T getOmegaFromCell(Cell<T,D> const& cell) const {
        PLB_PRECONDITION(yieldStress > -1e-15);
        PLB_PRECONDITION(plasticViscosity > 0);
        PLB_PRECONDITION(omegaMin > 0);
        
        Dynamics<T,D> const& dynamics=cell.getDynamics();
        
        T oldOmega = this->getBaseDynamics().getOmega();
        if (oldOmega < omegaMin - 1e-10) {
            oldOmega = 1;
        }

        T rhoBar;
        Array<T,D<T>::d> j;
        Array<T,SymmetricTensor<T,D>::n> PiNeq;
        dynamics.computeRhoBarJPiNeq(cell, rhoBar, j, PiNeq);
        
        T ic2 = D<T>::invCs2;
        T omega;
        if (yieldStress < (T)1e-18) {
            omega = (T)1 / (plasticViscosity * ic2 + (T)0.5);
        } else {
            if (PiNeq[0] == 0) {
                omega = 1;
            } else {
                T piNeqNorm = sqrt(SymmetricTensor<T,D>::tensorNormSqr(PiNeq));
                T rho = D<T>::fullRho(rhoBar);
                T om = ((T)1 - sqrt((T)2) * yieldStress * rho / piNeqNorm) / (plasticViscosity * ic2 + (T)0.5);
                if (om != om || om < omegaMin) {
                    omega = omegaMin;
                } else {
                    omega = om;
                }
            }
        }
        
        return 0.99 * oldOmega + 0.01 * omega;
    }
private:
    static int id;
};

template<typename T, template<typename U> class D>    
int BinghamDynamics<T,D>::id = meta::registerCompositeDynamics<T,D, BinghamDynamics<T,D> >("BinghamDynamics");

}
}
#endif 