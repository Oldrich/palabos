#ifdef linux
#include <sys/stat.h>
void makeDir(const char* name) { mkdir(name, 0777); }
#else
#include <direct.h>
void makeDir(const char* name) { mkdir(name); }
#endif