/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2013 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VTK_CELL_OUTPUT_HH
#define VTK_CELL_OUTPUT_HH

#include "vtkCellWriter3D.h"

#include "../Palabos/src/core/globalDefs.h"
#include "../Palabos/src/parallelism/mpiManager.h"
#include "../Palabos/src/dataProcessors/dataAnalysisWrapper2D.h"
#include "../Palabos/src/dataProcessors/dataAnalysisWrapper3D.h"
#include "../Palabos/src/dataProcessors/ntensorAnalysisWrapper2D.h"
#include "../Palabos/src/dataProcessors/ntensorAnalysisWrapper3D.h"
#include "../Palabos/src/io/serializerIO.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

namespace plb {

template<typename T>
class VtkImageCellOutput3D {
public:
    VtkImageCellOutput3D(std::string fName, double deltaX_=1.);
    VtkImageCellOutput3D(std::string fName, double deltaX_, Array<double,3> offset);
    ~VtkImageCellOutput3D();
    template<typename TConv>
    void writeData( plint nx, plint ny, plint nz, plint nDim,
                    DataSerializer const* serializer, std::string const& name );
    template<typename TConv>
    void writeData( Box3D boundingBox_, plint nDim,
                    DataSerializer const* serializer, std::string const& name );
    template<typename TConv>
    void writeData(ScalarField3D<T>& scalarField,
                   std::string scalarFieldName, TConv scalingFactor=(T)1);
    template<typename TConv>
    void writeData(MultiScalarField3D<T>& scalarField,
                   std::string scalarFieldName, TConv scalingFactor=(T)1);
    template<plint n, typename TConv>
    void writeData(TensorField3D<T,n>& tensorField,
                   std::string tensorFieldName, TConv scalingFactor=(T)1);
    template<plint n, typename TConv>
    void writeData(MultiTensorField3D<T,n>& tensorField,
                   std::string tensorFieldName, TConv scalingFactor=(T)1);
    template<typename TConv>
    void writeData(MultiNTensorField3D<T>& nTensorField, std::string nTensorFieldName);
private:
    void writeHeader(plint nx_, plint ny_, plint nz_);
    void writeHeader(Box3D boundingBox_);
    void writeFooter();
private:
    std::string fullName;
    VtkCellWriter3D vtkOut;
    double deltaX;
    Array<T,3> offset;
    bool headerWritten;
    Box3D boundingBox;
};

template<typename T>
VtkImageCellOutput3D<T>::VtkImageCellOutput3D(std::string fName, double deltaX_)
    : fullName ( global::directories().getVtkOutDir() + fName+".vti" ),
      vtkOut( fullName ),
      deltaX(deltaX_),
      offset(T(),T(),T()),
      headerWritten( false )
{ }

template<typename T>
VtkImageCellOutput3D<T>::VtkImageCellOutput3D(std::string fName, double deltaX_, Array<double,3> offset_)
    : fullName ( global::directories().getVtkOutDir() + fName+".vti" ),
      vtkOut( fullName ),
      deltaX(deltaX_),
      offset(offset_),
      headerWritten( false )
{ }

template<typename T>
VtkImageCellOutput3D<T>::~VtkImageCellOutput3D() {
    writeFooter();
}

template<typename T>
void VtkImageCellOutput3D<T>::writeHeader(plint nx_, plint ny_, plint nz_) {
    writeHeader(Box3D(0, nx_-1, 0, ny_-1, 0, nz_-1));
}

template<typename T>
void VtkImageCellOutput3D<T>::writeHeader(Box3D boundingBox_) {
    if (headerWritten) {
        PLB_PRECONDITION(boundingBox == boundingBox_);
    }
    else {
        boundingBox = boundingBox_;
        vtkOut.writeHeader(boundingBox, offset, deltaX);
        vtkOut.startPiece(boundingBox);
        headerWritten = true;
    }
}

template<typename T>
void VtkImageCellOutput3D<T>::writeFooter() {
    if (headerWritten) {
        vtkOut.endPiece();
        vtkOut.writeFooter();
        headerWritten = false;
    }
}


template<typename T>
template<typename TConv>
void VtkImageCellOutput3D<T>::writeData( plint nx, plint ny, plint nz, plint nDim,
                                     DataSerializer const* serializer,
                                     std::string const& name )
{
    writeHeader(nx, ny, nz);
    vtkOut.writeDataField<TConv> (serializer, name, nDim);
}

template<typename T>
template<typename TConv>
void VtkImageCellOutput3D<T>::writeData( Box3D boundingBox, plint nDim,
                                     DataSerializer const* serializer,
                                     std::string const& name )
{
    writeHeader(boundingBox);
    vtkOut.writeDataField<TConv> (serializer, name, nDim);
}

template<typename T>
template<typename TConv>
void VtkImageCellOutput3D<T>::writeData( ScalarField3D<T>& scalarField,
                                     std::string scalarFieldName, TConv scalingFactor )
{
    std::auto_ptr<ScalarField3D<TConv> > transformedField = copyConvert<T,TConv>(scalarField);
    multiplyInPlace(*transformedField, scalingFactor);
    writeData<TConv> (
            scalarField.getNx(), scalarField.getNy(), scalarField.getNz(), 1,
            transformedField->getBlockSerializer(transformedField->getBoundingBox(), IndexOrdering::backward),
            scalarFieldName );
}

template<typename T>
template<typename TConv>
void VtkImageCellOutput3D<T>::writeData( MultiScalarField3D<T>& scalarField,
                                     std::string scalarFieldName, TConv scalingFactor )
{
    std::auto_ptr<MultiScalarField3D<TConv> > transformedField = copyConvert<T,TConv>(scalarField);
    multiplyInPlace(*transformedField, scalingFactor);
    writeData<TConv> (
            scalarField.getBoundingBox(), 1,
            transformedField->getBlockSerializer(transformedField->getBoundingBox(), IndexOrdering::backward),
            scalarFieldName );
}

template<typename T>
template<plint n, typename TConv>
void VtkImageCellOutput3D<T>::writeData( TensorField3D<T,n>& tensorField,
                                     std::string tensorFieldName, TConv scalingFactor )
{
    std::auto_ptr<TensorField3D<TConv,n> > transformedField = copyConvert<T,TConv,n>(tensorField);
    multiplyInPlace(*transformedField, scalingFactor);
    writeData<TConv> (
            tensorField.getNx(), tensorField.getNy(), tensorField.getNz(), n,
            transformedField->getBlockSerializer(transformedField->getBoundingBox(), IndexOrdering::backward),
            tensorFieldName );
}

template<typename T>
template<plint n, typename TConv>
void VtkImageCellOutput3D<T>::writeData( MultiTensorField3D<T,n>& tensorField,
                                     std::string tensorFieldName, TConv scalingFactor )
{
    std::auto_ptr<MultiTensorField3D<TConv,n> > transformedField = copyConvert<T,TConv,n>(tensorField);
    multiplyInPlace(*transformedField, scalingFactor);
    writeData<TConv> (
            tensorField.getBoundingBox(), n,
            transformedField->getBlockSerializer(transformedField->getBoundingBox(), IndexOrdering::backward),
            tensorFieldName );
}


template<typename T>
template<typename TConv>
void VtkImageCellOutput3D<T>::writeData( MultiNTensorField3D<T>& nTensorField,
                                     std::string nTensorFieldName )
{
    MultiNTensorField3D<TConv>* transformedField = copyConvert<T,TConv>(nTensorField, nTensorField.getBoundingBox());
    writeData<TConv> (
            nTensorField.getBoundingBox(), nTensorField.getNdim(),
            transformedField->getBlockSerializer(transformedField->getBoundingBox(), IndexOrdering::backward),
            nTensorFieldName );
    delete transformedField;
}

}  // namespace plb

#endif  // VTK_DATA_OUTPUT_HH


