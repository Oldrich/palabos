#ifndef BINGHAM_FUNCTIONAL_H
#define BINGHAM_FUNCTIONAL_H

namespace plb {
    
template<typename T, template<typename U> class D>
class BinghamFunctional : public BoxProcessingFunctional3D_LS<T,D,int> {
public:
    BinghamFunctional(T plasticViscosity_, T yieldStress_, T omegaMin_):
    plasticViscosity(plasticViscosity_), yieldStress(yieldStress_), omegaMin(omegaMin_)
    {
        omega = 1;
    }
    
    BinghamFunctional(BinghamFunctional<T,D> const& rhs):
    plasticViscosity(rhs.plasticViscosity), yieldStress(rhs.yieldStress),
    omegaMin(rhs.omegaMin) {}
    
    BinghamFunctional<T,D>* clone() const { return new BinghamFunctional<T,D>(*this); }
    
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::nothing;
    }
    
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulk;
    }
    
    virtual void process(Box3D d, BlockLattice3D<T,D>& lt, ScalarField3D<int>& flags) {
        PLB_PRECONDITION(yieldStress > -1e-15);
        Dot3D ofs = computeRelativeDisplacement(lt, flags); 
        for (plint ix=d.x0; ix<=d.x1; ++ix) {
        for (plint iy=d.y0; iy<=d.y1; ++iy) {
        for (plint iz=d.z0; iz<=d.z1; ++iz) {
            Cell<T,D>& cell = lt.get(ix,iy,iz);
            Dynamics<T,D>& dynamics=cell.getDynamics();
            if (flags.get(ix+ofs.x, iy+ofs.y, iz+ofs.z) == 2) {
                T rhoBar;
                Array<T,D<T>::d> j;
                Array<T,SymmetricTensor<T,D>::n> PiNeq;
                dynamics.computeRhoBarJPiNeq(cell, rhoBar, j, PiNeq);

                T ic2 = D<T>::invCs2;
                if (yieldStress < (T)1e-18) {
                    omega = (T)1 / (plasticViscosity * ic2 + (T)0.5);
                } else {
                    if (PiNeq[0] == 0) {
                        omega = 1;
                    } else {
                        T piNeqNorm = sqrt(SymmetricTensor<T,D>::tensorNormSqr(PiNeq));
                        T rho = D<T>::fullRho(rhoBar);
                        T om = ((T)1 - sqrt((T)2) * yieldStress * rho / piNeqNorm) / (plasticViscosity * ic2 + (T)0.5);
                        if (om != om || om < omegaMin) {
                            //pcout << "omega=" << om << " piNeq=" << piNeqNorm << " rho=" << rho << std::endl;
                            omega = omegaMin;
                        } else {
                            omega = om;
                        }
                    }
                }
            }
            dynamics.setOmega(omega);
        }}}
    }
    T plasticViscosity, yieldStress, omegaMin;
private:
    T omega;
};

template<typename T, template<typename U> class D>
void integrateBinghamFunctional(
    T plasticViscosity, T yieldStress, T omegaMin,
    FreeSurfaceFields3D<T,D>& fs ) {
    integrateProcessingFunctional(
        new BinghamFunctional<T,D>(plasticViscosity, yieldStress, omegaMin),
        fs.lattice.getBoundingBox(), fs.lattice, fs.flag, 0);
}

template<typename T, template<typename U> class D>
void integrateBinghamFunctional(
    T plasticViscosity, T yieldStress, T omegaMin,
    TwoPhaseFields3D<T,D>& fs ) {
    integrateProcessingFunctional(
        new BinghamFunctional<T,D>(plasticViscosity, yieldStress, omegaMin),
        fs.lattice.getBoundingBox(), fs.lattice, fs.flag, 0);
}

}
#endif
