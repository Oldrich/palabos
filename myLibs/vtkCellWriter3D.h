/* This file is part of the Palabos library.
 *
 * Copyright (C) 2011-2013 FlowKit Sarl
 * Route d'Oron 2
 * 1010 Lausanne, Switzerland
 * E-mail contact: contact@flowkit.com
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef VTK_CELL_OUTPUT_H
#define VTK_CELL_OUTPUT_H

#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "../Palabos/src/core/globalDefs.h"
#include "../Palabos/src/core/serializer.h"
#include "../Palabos/src/core/serializer.hh"
#include "../Palabos/src/io/serializerIO.h"
#include "../Palabos/src/atomicBlock/dataField2D.h"
#include "../Palabos/src/multiBlock/multiDataField2D.h"
#include "../Palabos/src/atomicBlock/dataField3D.h"
#include "../Palabos/src/multiBlock/multiDataField3D.h"
#include "../Palabos/src/core/array.h"
#include "../Palabos/src/parallelism/mpiManager.h"
#include "../Palabos/src/io/base64.h"
#include "../Palabos/src/io/base64.hh"

namespace plb {

class VtkCellWriter3D {
public:
    VtkCellWriter3D(std::string const& fileName_)
    : fileName(fileName_),
      ostr(0)
    {
        if (global::mpi().isMainProcessor()) {
            ostr = new std::ofstream(fileName.c_str());
            if (!(*ostr)) {
                std::cerr << "could not open file " <<  fileName << "\n";
                return;
            }
        }
    }
    
    ~VtkCellWriter3D(){
        delete ostr;
    }
    
    void writeHeader(Box3D domain, Array<double,3> origin, double deltaX) {
        if (global::mpi().isMainProcessor()) {
            (*ostr) << "<?xml version=\"1.0\"?>\n";
#ifdef PLB_BIG_ENDIAN
        (*ostr) << "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"BigEndian\">\n";
#else
        (*ostr) << "<VTKFile type=\"ImageData\" version=\"0.1\">\n";
#endif
            (*ostr) << "<ImageData WholeExtent=\""
                    << domain.x0 << " " << domain.x1 + 1 << " "
                    << domain.y0 << " " << domain.y1 + 1 << " "
                    << domain.z0 << " " << domain.z1 + 1 << "\" "
                    << "Origin=\""
                    << origin[0] << " " << origin[1] << " " << origin[2] << "\" "
                    << "Spacing=\""
                    << deltaX << " " << deltaX << " " << deltaX << "\">\n";
        }
    }
    
    void startPiece(Box3D domain){
        if (global::mpi().isMainProcessor()) {
            (*ostr) << "<Piece Extent=\""
                    << domain.x0 << " " << domain.x1 + 1 << " "
                    << domain.y0 << " " << domain.y1 + 1 << " "
                    << domain.z0 << " " << domain.z1 + 1 << "\">\n";
            (*ostr) << "<CellData>\n";
        }
    }
    
    void endPiece() {
        if (global::mpi().isMainProcessor()) {
            (*ostr) << "</CellData>\n";
            (*ostr) << "</Piece>\n";
        }
    }

    void writeFooter(){
        if (global::mpi().isMainProcessor()) {
            (*ostr) << "</ImageData>\n";
            (*ostr) << "</VTKFile>\n";
        }
    }
    
    template<typename T>
    void writeDataField( DataSerializer const* serializer,
                         std::string const& name, plint nDim ){
        if (global::mpi().isMainProcessor()) {
            (*ostr) << "<DataArray type=\"" << VtkTypeNames<T>::getName()
                    << "\" Name=\"" << name
                    << "\" format=\"ascii";
            if (nDim>1) {
                (*ostr) << "\" NumberOfComponents=\"" << nDim;
            }
            (*ostr) << "\">\n";
        }

        // Undocumented requirement of the vtk xml file format:
        // in front of every binary blob, base64 or raw-binary, appended or not, 
        // there is an UInt32 length indicator, giving the size of the binary blob in bytes;
        // when using base64 encoding, that length header must be encoded separately;
        // there must be no newline between the encoded length indicator and the encoded data block.

        //bool enforceUint=true; // VTK uses "unsigned" to indicate the size of data, even on a 64-bit machine.
        //serializerToAsciiStream(serializer, ostr, 6);
        
        serializerToSink(serializer, new AsciiWriter<T>(ostr, 4));
        //serializerToAsciiStream(serializer, ostr, (plint)6);

        if (global::mpi().isMainProcessor()) {
            (*ostr) << "\n</DataArray>\n";
        }
    }
private:
    VtkCellWriter3D(VtkCellWriter3D const& rhs);
    VtkCellWriter3D operator=(VtkCellWriter3D const& rhs);
private:
    std::string fileName;
    std::ofstream *ostr;
};

} // namespace plb

#endif  // VTK_CELL_OUTPUT_H 